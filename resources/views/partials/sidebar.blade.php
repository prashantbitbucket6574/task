<aside class="main-sidebar" style="background-color: #1b1e21">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Manage Users</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('userList')}}"><i class="fas fa-list-ol"></i> List</a></li>
                </ul>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

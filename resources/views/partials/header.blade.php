<header class="main-header">

    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.5.0/css/all.css' integrity='sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU' crossorigin='anonymous'>


    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>Tas</b>k</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>TA</b>SK</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
        <div class="col-md-offset-10">
            <a href="{{route('logout')}}" class="btn btn-info btn-lg"style="margin-left: 60px; background-color: #3c8dbc; border: none;">
                <span class="glyphicon glyphicon-log-out"></span> Log out
            </a>
        </div>

    </nav>

</header>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">User Detail</h4>
            </div>
            <div class="modal-body">
                <p><b>User Name:</b> <span class="user_name"></span></p>
                <p><b>User Email:</b> <span class="user_email"></span></p>
                <p><b>User Email Verified At:</b> <span class="email_veri_at"></span></p>
                <p><b>User Created At:</b> <span class="email_cre_at"></span></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
      
    </div>
</div>
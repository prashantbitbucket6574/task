@extends('layouts.admin')
@section('title', 'User List')

@section('content')
    <div class="content-wrapper">

        <section class="content">

            <div class=" content-body">
                <div class="content-body-section">
                    @if($errors->count() > 0)
                        @foreach($errors->all() as $error)
                            <p class="alert alert-danger">{{$error}}</p>
                        @endforeach
                    @endif
                    @if(Session::has('message'))
                        <div class="alert alert-success"> {{Session::get('message')}}</div>
                    @endif
                    @if(Session::has('error'))
                        <div class="alert alert-danger"> {{Session::get('error')}}</div>
                    @endif

                    <div class="alert alert-danger java_error" style="display: none;"> </div>
                        

                    <form name="filter" >
                        {{csrf_field()}}
                        <div class="row justify-content-between">

                            <div class="form-group col-xs-2 no-col-2">
                                <label>Enter Name & Email</label>
                                <input type="text" id="name_email" name="name_email" class="form-control main_input" value="" placeholder="Enter Name & Email">
                            </div>

                            <div class="form-group" style="margin-top: 25px;">
                                <span class="input-group-btn" style="width: 0px;">
                                    <button class="btn btn-block btn-default z-index-0" type="submit" id="user-search-btn">Search</button>
                                </span>
                            </div>

                        </div>
                    </form>
                    <div class="row pt-20">
                        <div class="table-responsive">
                            <table class="table" name="userList" id="myTable">
                                <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Time</th>
                                    <th>View Profile Detail</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @if(count($userList) > 0)
                                        @foreach($userList as $key => $value)
                                            <tr>
                                                <td>{{ $loop->index+1 }}</td>
                                                <td>{{ $value->name }}</td>
                                                <td>{{ $value->email }}</td>
                                                <td>{{ $value->created_at }}</td>
                                                <td>
                                                    <a href="javascript:void(0)" class="btn btn-info user_detail" title="Click To View User Detail" user_id-attr="{{ $value->id }}"><i class="fa fa-eye"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="10" style="text-align: center; font-size: 30px;">No User Details Available </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box -->
                        <div id="paginate">
                            {{$userList->links()}}                        
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

@section('scripts')
<script type="text/javascript" src="{{asset('js/user.js')}}"></script>
@endsection
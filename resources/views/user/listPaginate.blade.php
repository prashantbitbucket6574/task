@if(count($userList) > 0)
    @foreach($userList as $key => $value)
        <tr>
            <td>{{ 10*($page-1) + ($loop->index+1) }}</td>
            <td>{{ $value->name }}</td>
            <td>{{ $value->email }}</td>
            <td>{{ $value->created_at }}</td>
            <td>
                <a href="javascript:void(0)" class="btn btn-info user_detail" title="Click To View User Detail" user_id-attr="{{ $value->id }}"><i class="fa fa-eye"></i></a>
            </td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="10" style="text-align: center; font-size: 30px;">No Users Available </td>
    </tr>
@endif
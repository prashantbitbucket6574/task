<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Task @yield('title')</title>
        @include('partials.head')
    </head>
    <body class="skin-blue" data-baseurl="{{asset('/')}}">
        <div class="wrapper">
            @include('partials.header')

            @include('partials.sidebar')

            @yield('content')

            <footer class="main-footer">
                @include('partials.footerScripts')
            </footer>
            @yield('scripts')
        </div>
    </body>
</html>
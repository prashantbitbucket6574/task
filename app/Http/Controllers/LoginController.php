<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

class LoginController extends Controller
{
    public function login(Request $request){
    	// dd($request);
    	$this->validate($request, [
    		'email' => 'required|email|exists:users,email',
    		'password' => 'required'
    	]);
    	// dd(Auth::attempt(['email' => $request->email,'password' => $request->password]));
    	if(Auth::attempt(['email' => $request->email,'password' => $request->password])){
    		// dd('hii');
    		return redirect()->route('userList');
    	}else{
    		return redirect()->back()->with('error' , 'Email and password does not match');
    	}
    }

    public function logout(){
    	Auth::logout();
    	return redirect()->route('a.login');
    }
}

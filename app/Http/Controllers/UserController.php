<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class UserController extends Controller
{
    public function userList(){
    	$userList = User::orderBy('updated_at', 'desc')->paginate(10);

    	return view('user.userList', compact('userList'));
    }

    public function getUserList($name_email = null){
    	$userList  = User::orderBy('updated_at', 'desc');
    	if($name_email != null){
    		$userList = $userList->where('name', 'like', '%'.$name_email.'%')->orWhere('email', 'like', '%'.$name_email.'%');
    	}
    	$userList  = $userList->paginate(10);
    	$page = $_GET['page'];

    	$data['view'] = html_entity_decode(view('user.listPaginate', compact('userList','page')));
    	$data['paginate'] = html_entity_decode(view('user.paginate', compact('userList')));

    	return response()->json($data, 200);
    }

    public function userDetail($id){
    	$user = User::where('id', $id)->first();

    	if($user == null){
    		$error = 'Oops...User not found.';
    		return response()->json($error, 400);
    	}
    	return response()->json($user, 200);
    }
}

$(document).ready(function(){
	var base_url = $('body').attr('data-baseurl');
	// console.log(base_url,'jioooooooooooo');
    $('body').on('click', '.page-item', function (e){
        e.preventDefault();
        if($(this).hasClass('active')){
        	console.log('yes');
        	return ;
        }
        if($(this).hasClass('disabled')){
        	console.log('yes');
        	return ;
        }

        var name_email = $('#name_email').val();

        var next_url = $(this).find('.page-link').attr('href');
        next_url = next_url.split('?');
        $.ajax({
        	type: 'GET',
        	url: base_url + 'get-user-list/'+name_email+'?'+next_url[1],
        	success: function(data){
        		$('.java_error').hide();
        		console.log(data);
        		$('#myTable').find('tbody').empty();
        		$('#myTable').find('tbody').append(data.view);

        		$('#paginate').empty();
        		$('#paginate').append(data.paginate);
        	},
        	error: function(error){
        		console.log(error);
        	}
        });
        console.log(next_url, 'ooooo');
    });

    // For search user
    $('#user-search-btn').on('click', function(e){
    	e.preventDefault();
    	var name_email = $('#name_email').val();

    	$.ajax({
    		type: 'GET',
    		url: base_url + 'get-user-list/'+name_email+'?page=1',
    		success: function(data){
    			$('.java_error').hide();
        		console.log(data);
        		$('#myTable').find('tbody').empty();
        		$('#myTable').find('tbody').append(data.view);

        		$('#paginate').empty();
        		$('#paginate').append(data.paginate);
        	},
        	error: function(error){
        		console.log(error, 'error');
        	}
    	});
    });

    // For user detail
    $('body').on('click','.user_detail', function(){
    	var user_id = $(this).attr('user_id-attr');
    	$.ajax({
    		type: 'GET',
    		url: base_url+'user-detail/'+user_id,
    		success: function(data){
    			$('.java_error').hide();
    			console.log(data);
    			$('.user_name').text('');
    			$('.user_email').text('');
    			$('.email_veri_at').text('');
    			$('.email_cre_at').text('');
    			$('.user_name').text(data.name);
    			$('.user_email').text(data.email);
    			$('.email_veri_at').text(data.email_verified_at);
    			$('.email_cre_at').text(data.created_at);
    			$('#myModal').modal('show');
    		},
    		error: function(error){
    			console.log(error);
    			$('.java_error').text('');
    			$('.java_error').text(error.responseJSON);
    			$('.java_error').show();
    		}
    	});
    });


});
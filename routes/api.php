<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'user'], function(){
	Route::post('sign-up', 'User\RegisterController@signUp');
	Route::post('sign-in', 'User\RegisterController@signIn');
	Route::post('forget-password','User\RegisterController@passwordForgot');
	Route::post('forget-password/otp-verify','User\RegisterController@otpVerify');
	Route::post('forget-password/change-password','User\RegisterController@changePassword');
	Route::post('profile-creation','User\ProfileController@createProfile')->middleware('auth:api');

	// {  trip start

	//create Trip
	Route::get('get-trip-category', 'User\TripController@getTripCategory')->middleware('auth:api');
	Route::post('create-trip','User\TripController@createTrip')->middleware('auth:api');
	// Get all trip of a user
	Route::get('get-trip/{user_id}/{type}/{page_no?}','User\TripController@getTrip')->middleware('auth:api');
	Route::get('get-trip-by-city/{city}/{page_no?}','User\TripController@getTripByCity')->middleware('auth:api');
	// trip comment/ subcomment on trip
	Route::post('post-trip-comment','User\TripController@postTripComment')->middleware('auth:api');
	Route::post('post-trip-sub-comment','User\TripController@postTripSubComment')->middleware('auth:api');
	Route::get('get-trip-comments/{trip_id}/{page_no}', 'User\TripController@loadMoreComments')->middleware('auth:api');
	Route::get('get-user-latest-trip/{user_id}', 'User\TripController@userLatestTrip')->middleware('auth:api');
	// Trip like/unlike 
	Route::post('trip-like-unlike','User\TripController@tripLikeUnlike')->middleware('auth:api');

	//like trip comment
	Route::post('trip-comment-like','User\TripController@tripCommentLike')->middleware('auth:api');
	//unlike trip comment
	Route::post('trip-comment-unlike','User\TripController@tripCommentUnlike')->middleware('auth:api');

	// Trip delete
	Route::post('trip-delete','User\TripController@tripDelete')->middleware('auth:api');

	//Trip timeline 
	Route::get('get-trip-timeline/{trip_id}/{page_no?}', 'User\TripController@getTripTimeline')->middleware('auth:api');

	// Add to favourite
	Route::post('trip-add-to-favourite','User\TripController@tripAddToFav')->middleware('auth:api');
	Route::post('trip-remove-from-favourite','User\TripController@tripRemoveFromFav')->middleware('auth:api');

	// Add to wishlist
	Route::post('trip-add-to-wishlist','User\TripController@tripAddToWish')->middleware('auth:api');
	Route::post('trip-remove-from-wishlist','User\TripController@tripRemoveFromWish')->middleware('auth:api');

	// Follow Trip
	Route::post('trip-follow','User\TripController@tripFollow')->middleware('auth:api');
	Route::post('trip-unfollow','User\TripController@tripUnfollow')->middleware('auth:api');

	// Add memory to trip 
	Route::post('create-trip-memory','User\TripController@createTripMemory')->middleware('auth:api');

	// trip edit
	Route::post('trip-edit','User\TripController@editTrip')->middleware('auth:api');

	// Customize trip (Plan of Trip)
	Route::get('get-customize-trip/{trip_id}','User\TripController@getCustomizeTrip')->middleware('auth:api');
	Route::post('add-customize-trip','User\TripController@addPlan')->middleware('auth:api');
	Route::post('edit-customize-trip','User\TripController@editPlan')->middleware('auth:api');
	Route::post('remove-customize-trip','User\TripController@removePlan')->middleware('auth:api');

	// Copy trip ===>>> murtu bhai ko updated
	Route::post('copy-trip','User\TripController@copyTrip')->middleware('auth:api');

	// My Bag Apis
	Route::get('my-check-ins/{user_id}/{page_no?}', 'User\MyBagController@myCheckIns')->middleware('auth:api');
	Route::get('country-visited/{user_id?}', 'User\MyBagController@countryVisited')->middleware('auth:api');  // This can be also used for visa 
	// Route::get('favourite-cities/{user_id?}', 'User\MyBagController@citiesFavourite')->middleware('auth:api');   yet to complete
	Route::get('get-visa/{user_id?}', 'User\MyBagController@userVisa')->middleware('auth:api');
	Route::get('get-souvenir/{user_id?}', 'User\MyBagController@userSouvenir')->middleware('auth:api');

	// My friends
	Route::get('get-friends/{name}', 'User\FriendController@getAllFriends')->middleware('auth:api');
	Route::get('my-friends/{page_no}', 'User\FriendController@getMyFriends')->middleware('auth:api');
	Route::get('is-channel-exists/{friend_id}', 'User\FriendController@isChannelExist')->middleware('auth:api');
	

	// This is for second tab
	Route::get('get-cities/{page_no?}', 'User\SecondTabController@getCities')->middleware('auth:api');
	Route::get('friend-data/{page_no?}', 'User\SecondTabController@getFriendsFeed')->middleware('auth:api');
	Route::get('friend-experience-data/{page_no?}', 'User\SecondTabController@getFriendsExperience')->middleware('auth:api');
	Route::get('trending-data/{page_no?}', 'User\SecondTabController@trendingFeed')->middleware('auth:api');
	Route::get('explore-data/{page_no?}', 'User\SecondTabController@exploreFeed')->middleware('auth:api');
	// This is for making any memory to our customization list
	Route::get('get-trips-for-making-others-memory-to-our-customization-list/{memory_id}', 'User\SecondTabController@getAllTripForCustomization')->middleware('auth:api');
	Route::post('making-others-memory-to-our-customization-list', 'User\SecondTabController@addToPlan')->middleware('auth:api');

	// Get memories comment
	Route::get('get-memory-comments/{memory_id}/{page_no}', 'User\SecondTabController@loadMemoryComments')->middleware('auth:api');

	// This is for fourth tab
	Route::get('get-all-trip-coustomized/{page_no?}', 'User\FourthController@getFourthTabTrips')->middleware('auth:api');


	// Memory related api's
	Route::post('memory-like-unlike', 'User\TripController@memoryLikeUnlike')->middleware('auth:api');
	Route::post('post-memory-comment', 'User\TripController@postMemoryComment')->middleware('auth:api');
	Route::post('post-memory-sub-comment', 'User\TripController@postMemorySubComment')->middleware('auth:api');
	Route::post('memory-comment-like', 'User\TripController@memoryCommentLike')->middleware('auth:api');
	Route::post('memory-comment-unlike','User\TripController@memoryCommentUnlike')->middleware('auth:api');
	Route::post('memory-delete','User\TripController@memoryDelete')->middleware('auth:api');
	

	// Experience relate api's
	Route::post('experience-folder-create', 'User\ExperienceController@createExperienceFolder')->middleware('auth:api');

	Route::post('experience-like-unlike', 'User\ExperienceController@experienceLikeUnlike')->middleware('auth:api');
	Route::post('post-experience-comment', 'User\ExperienceController@postExperienceComment')->middleware('auth:api');
	Route::post('post-experience-sub-comment', 'User\ExperienceController@postExperienceSubComment')->middleware('auth:api');
	Route::post('experience-comment-like', 'User\ExperienceController@experienceCommentLike')->middleware('auth:api');
	Route::post('experience-comment-unlike','User\ExperienceController@experienceCommentUnlike')->middleware('auth:api');
	Route::post('experience-delete','User\ExperienceController@experienceDelete')->middleware('auth:api');
	


	// Third tab experiences
	Route::get('get-my-folders/{page_no?}', 'User\ThirdController@getMyFolders')->middleware('auth:api');
	Route::get('get-my-experience/{folder_id}/{page_no?}', 'User\ThirdController@getMyExperience')->middleware('auth:api');

	Route::get('get-all-experience-categories', 'User\ThirdController@getAllcategoryExperience')->middleware('auth:api');
	Route::get('get-all-experiences-folder/{category_id}/{page_no?}', 'User\ThirdController@getAllExperience')->middleware('auth:api');
	Route::get('get-all-experiences/{folder_id}/{page_no?}', 'User\ThirdController@getExperience')->middleware('auth:api');
	// Get memories comment
	Route::get('get-experience-comments/{experience_id}/{page_no}', 'User\ThirdController@loadExperienceComments')->middleware('auth:api');


	// Main search
	Route::get('main-search/{key}', 'User\SearchController@mainSearch')->middleware('auth:api');
	// }  trip end

	// For chat
	// Route::get('panel/chat','User\ChatController@chatView')->name('panel.chat')->middleware('auth:api');
	Route::get('chat-section/user-lists', 'User\ChatController@listLoad')->middleware('auth:api');
	Route::post('load-chat', 'User\ChatController@rightChatSection')->middleware('auth:api');
	// This is for sending message
	Route::post('send-message', 'User\ChatController@sendMessage')->name('sendMessage')->middleware('auth:api');
	// This is for sending docs
	Route::post('send-docs','User\ChatController@sendDocs')->middleware('auth:api');
	// This is for load prevoius chat in single chat
	Route::post('load-previous-message','User\ChatController@loadPreviousMessage')->middleware('auth:api');
	// This is for getting all friends for chat
	Route::get('get-all-friends-for-chat', 'User\ChatController@allFriendsForChat')->middleware('auth:api');
	// This is for searched user and chat
	Route::get('click-to-chat-one-to-one','User\ChatController@oneToOneConver')->middleware('auth:api');		// DOUBT
	//This is for modal one to one msg send
	Route::post('one-to-one/message-send', 'User\ChatController@firstMessageSend')->middleware('auth:api');
	//This is for single chat
	Route::get('panel/single-chat/{channel_id}', 'User\ChatController@singleChat')->name('singleChat')->middleware('auth:api');
	//This is for pagination in chat panel
	Route::post('user-list-paginate', 'User\ChatController@userListPaginate')->middleware('auth:api');

	// End Chat

	// Story related api's
	Route::get('get-friend-trip-story/{page_no?}', 'User\StoryController@getTripStory')->middleware('auth:api');
	Route::get('get-all-story/{user_id}', 'User\StoryController@getFriendAllStory')->middleware('auth:api');
	Route::post('add-story', 'User\StoryController@addStory')->middleware('auth:api');

	Route::get('story-acc-to-trip/{trip_id}', 'User\StoryController@getStoryAccToTrip')->middleware('auth:api');

	// Notification related api's
	Route::get('get-all-notification/{page_no?}', 'User\NotificationController@getNotification')->middleware('auth:api');

	// Send friend request
	Route::post('send-friend-request','User\FriendController@sendRequest')->middleware('auth:api');
	// Accept friend request
	Route::post('accept-friend-request','User\FriendController@acceptRequest')->middleware('auth:api');
	// Unfriend a friend
	Route::get('remove-friend/{user_id}','User\FriendController@unfriend')->middleware('auth:api');
	// Friend Suggestion
	Route::get('friends-suggestion/{page_no?}', 'User\FriendController@suggestion')->middleware('auth:api');

	// Create Experience
	Route::post('create-experience','User\ExperienceController@createExperience')->middleware('auth:api');

	// Get tagged peoples list
	Route::get('get-tagged-peoples/{trip_id}/{page_no}', 'User\TripController@getTaggedPeoples')->middleware('auth:api');

	// Get People of fly high (any category either Trip, Experience and Memory)
	Route::get('get-peoples-fly-high/{id}/{type}/{page_no?}', 'User\CommonController@getFlyHighPeoples')->middleware('auth:api');

	// Delete Comment
	Route::get('comment-delete/{id}', 'User\CommonController@deleteComment')->middleware('auth:api');

	// Edit comment
	Route::get('comment-edit/{id}', 'User\CommonController@editComment')->middleware('auth:api');
	Route::post('comment-update/{id}', 'User\CommonController@updateComment')->middleware('auth:api');

	// Delete SUb Comment
	Route::get('subcomment-delete/{id}', 'User\CommonController@deleteSubComment')->middleware('auth:api');

	// Edit sub comment
	Route::get('sub-comment-edit/{id}', 'User\CommonController@editSubComment')->middleware('auth:api');
	Route::post('sub-comment-update/{id}', 'User\CommonController@updateSubComment')->middleware('auth:api');


	// Api to check contact's invite
	Route::post('check-for-invite', 'User\FriendController@checkFriendInvite')->middleware('auth:api');
});
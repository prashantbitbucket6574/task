<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
})->middleware('guest')->name('a.login');

Route::post('login','LoginController@login')->name('login');
Route::get('logout','LoginController@logout')->name('logout');

Route::get('user-list','UserController@userList')->middleware('auth')->name('userList');
Route::get('get-user-list/{name_email?}', 'UserController@getUserList')->middleware('auth');
Route::get('user-detail/{id}', 'UserController@userDetail')->middleware('auth');